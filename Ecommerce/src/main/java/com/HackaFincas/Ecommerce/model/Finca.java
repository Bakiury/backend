package com.HackaFincas.Ecommerce.model;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="finca")
public class Finca implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Integer id;
    @Column
    private String nombre;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "codigoubicacion")
    private Ubicacion codigoubicacion;
    @Column
    private double extension;
    @Column
    private double imagen;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "pedidos")
    private ArrayList<Pedido> pedidos;

    public Finca() {
    }

    public Finca(Integer id, String nombre, Ubicacion codigoubicacion, double extension, double imagen) {
        this.id = id;
        this.nombre = nombre;
        this.codigoubicacion = codigoubicacion;
        this.extension = extension;
        this.imagen = imagen;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Ubicacion getCodigoubicacion() {
        return codigoubicacion;
    }

    public void setCodigoubicacion(Ubicacion codigoubicacion) {
        this.codigoubicacion = codigoubicacion;
    }

    public double getExtension() {
        return extension;
    }

    public void setExtension(double extension) {
        this.extension = extension;
    }

    public double getImagen() {
        return imagen;
    }

    public void setImagen(double imagen) {
        this.imagen = imagen;
    }
}
