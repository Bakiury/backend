package com.HackaFincas.Ecommerce.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "inventario")
public class Inventario implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Integer id;
    @OneToMany
    @Column
    private Producto codigoProducto;
    @Column
    @OneToMany
    private Finca codigoFinca;
    @Column
    private Integer stock;
    @Column
    private double precio;
    @Column
    private boolean a_ofertar;
    @Column
    private boolean a_aceptar;


    public Inventario() {
    }


    public Inventario(Integer id, Producto codigoProducto, Finca codigoFinca, Integer stock, double precio, boolean a_ofertar, boolean a_aceptar) {
        this.id = id;
        this.codigoProducto = codigoProducto;
        this.codigoFinca = codigoFinca;
        this.stock = stock;
        this.precio = precio;
        this.a_ofertar = a_ofertar;
        this.a_aceptar = a_aceptar;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Producto getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(Producto codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public Finca getCodigoFinca() {
        return codigoFinca;
    }

    public void setCodigoFinca(Finca codigoFinca) {
        this.codigoFinca = codigoFinca;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public boolean isA_ofertar() {
        return a_ofertar;
    }

    public void setA_ofertar(boolean a_ofertar) {
        this.a_ofertar = a_ofertar;
    }

    public boolean isA_aceptar() {
        return a_aceptar;
    }

    public void setA_aceptar(boolean a_aceptar) {
        this.a_aceptar = a_aceptar;
    }
}
