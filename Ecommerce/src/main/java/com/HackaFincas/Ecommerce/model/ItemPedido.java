package com.HackaFincas.Ecommerce.model;

import java.io.Serializable;

import javax.persistence.*;
@Entity
@Table(name = "itemPedido")
public class ItemPedido implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Integer id;
    @OneToMany
    @Column
    private Inventario codigoInventario;
    @OneToMany
    @Column
    private Pedido codigoPedido;
    @Column
    private double precio;
    @Column
    private Integer cantidad;
    @Column
    private double subtotal;


    public ItemPedido() {
    }


    public ItemPedido(Integer id, Inventario codigoInventario, Pedido codigoPedido, double precio, Integer cantidad, double subtotal) {
        this.id = id;
        this.codigoInventario = codigoInventario;
        this.codigoPedido = codigoPedido;
        this.precio = precio;
        this.cantidad = cantidad;
        this.subtotal = subtotal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Inventario getCodigoInventario() {
        return codigoInventario;
    }

    public void setCodigoInventario(Inventario codigoInventario) {
        this.codigoInventario = codigoInventario;
    }

    public Pedido getCodigoPedido() {
        return codigoPedido;
    }

    public void setCodigoPedido(Pedido codigoPedido) {
        this.codigoPedido = codigoPedido;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }
}
