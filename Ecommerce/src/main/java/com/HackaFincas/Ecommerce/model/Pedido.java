package com.HackaFincas.Ecommerce.model;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "pedido")
public class Pedido implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Integer id;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "codigoCliente")
    private Persona codigoCliente;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "codigoFinca")
    private Finca codigoFinca;
    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date fecha;
    @Column
    private double totalVenta;


    public Pedido() {
    }


    public Pedido(Integer id, Persona codigoCliente, Finca codigoFinca, Date fecha, double totalVenta) {
        this.id = id;
        this.codigoCliente = codigoCliente;
        this.codigoFinca = codigoFinca;
        this.fecha = fecha;
        this.totalVenta = totalVenta;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Persona getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(Persona codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public Finca getCodigoFinca() {
        return codigoFinca;
    }

    public void setCodigoFinca(Finca codigoFinca) {
        this.codigoFinca = codigoFinca;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public double getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(double totalVenta) {
        this.totalVenta = totalVenta;
    }
}
