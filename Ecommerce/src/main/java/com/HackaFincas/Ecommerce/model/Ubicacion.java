package com.HackaFincas.Ecommerce.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="ubicacion")
public class Ubicacion implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Integer id;
    @Column
    private String departamento;
    @Column
    private String municipio;
    @Column
    private String direccion;
    @Column
    private String detalles;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "codigoAgricultor")
    private Persona codigoAgricultor;

    public Ubicacion() {
    }

    public Ubicacion(Integer id, String departamento, String municipio, String direccion, String detalles, Persona codigoAgricultor) {
        super();
        this.id = id;
        this.departamento = departamento;
        this.municipio = municipio;
        this.direccion = direccion;
        this.detalles = detalles;
        this.codigoAgricultor = codigoAgricultor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

    public Persona getCodigoAgricultor() {
        return codigoAgricultor;
    }

    public void setCodigoAgricultor(Persona codigoAgricultor) {
        this.codigoAgricultor = codigoAgricultor;
    }
}
