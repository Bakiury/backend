package com.HackaFincas.Ecommerce.repository;

import com.HackaFincas.Ecommerce.model.Finca;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FincaRepository extends JpaRepository<Finca,Integer> {
    public Finca findFincaById(Integer id);
}
