package com.HackaFincas.Ecommerce.repository;

import com.HackaFincas.Ecommerce.model.Ubicacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UbicacionRepository extends JpaRepository<Ubicacion, Integer> {
    
}
